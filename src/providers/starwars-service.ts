import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the StarwarsService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class StarwarsService {

	items:  Array<{title: string, note: string, icon: string}>;
	url: string;

static get parameters() {
    return [[Http]];
}
  	constructor(public http: Http) {
    	console.log('Hello StarwarsService Provider');
    	this.url = 'http://swapi.co/api/people/';

    	this.items = [];
		this.items.push({title: "One", note: "1", icon: "None"});
		this.items.push({title: "Two", note: "1", icon: "None"});
		this.items.push({title: "Three", note: "1", icon: "None"});
		this.items.push({title: "Four", note: "1", icon: "None"});
		this.items.push({title: "Five", note: "1", icon: "None"});
		this.items.push({title: "Six", note: "1", icon: "None"});
		this.items.push({title: "Seven", note: "1", icon: "None"});
		this.items.push({title: "Eight", note: "1", icon: "None"});
		this.items.push({title: "Nine", note: "1", icon: "None"});
		this.items.push({title: "Ten", note: "1", icon: "None"});
				this.items.push({title: "Five", note: "1", icon: "None"});
		this.items.push({title: "Six", note: "1", icon: "None"});
		this.items.push({title: "Seven", note: "1", icon: "None"});
		this.items.push({title: "Eight", note: "1", icon: "None"});
		this.items.push({title: "Nine", note: "1", icon: "None"});
		this.items.push({title: "Ten", note: "1", icon: "None"});
				this.items.push({title: "Five", note: "1", icon: "None"});
		this.items.push({title: "Six", note: "1", icon: "None"});
		this.items.push({title: "Seven", note: "1", icon: "None"});
		this.items.push({title: "Eight", note: "1", icon: "None"});
		this.items.push({title: "Nine", note: "1", icon: "None"});
		this.items.push({title: "Ten", note: "1", icon: "None"});
				this.items.push({title: "Five", note: "1", icon: "None"});
		this.items.push({title: "Six", note: "1", icon: "None"});
		this.items.push({title: "Seven", note: "1", icon: "None"});
		this.items.push({title: "Eight", note: "1", icon: "None"});
		this.items.push({title: "Nine", note: "1", icon: "None"});
		this.items.push({title: "Ten", note: "1", icon: "None"});
				this.items.push({title: "Five", note: "1", icon: "None"});
  }

  	getPerson(idx)
  	{
  		let fullUrl = this.url+idx+"/";
  		console.log(fullUrl);
  		let info = this.http.get(fullUrl)
  			.map(res => res.json());
  		console.log(info);
  
  		return info;
  	}

  	load(person_id)
  	{
  		let fullUrl = this.url+person_id+"/";
  			console.log(fullUrl);
  		let info = this.http.get(fullUrl)
  			.map(res => res.json());

  		return info;
  	}

  	getItems()
	{
		return this.items;
	}

}
