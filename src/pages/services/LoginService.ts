export class LoginService {
	loggedIn: boolean;
	construct()
	{
		this.loggedIn = false;
	}

	setLoggedIn(value)
	{
		this.loggedIn = value;
	}

	getLoggedIn(value)
	{
		return this.loggedIn;
	}
}