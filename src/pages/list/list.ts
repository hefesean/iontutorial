import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';

import { ItemDetailsPage } from '../item-details/item-details';

import { StarwarsService } from '../../providers/starwars-service';


@Component({
    selector: 'page-list',
    templateUrl: 'list.html'
})
export class ListPage {
    selectedItem: any;
    icons: string[];
    items: Array<{title: string, note: string, icon: string}>;
    starwarsService : any;
    windowCount: number;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    starwarsService: StarwarsService) {
    this.windowCount = 0;
     this.starwarsService = starwarsService;
    // If we navigated to this page, we will have an item available as a nav param
    this.selectedItem = navParams.get('item');

    this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
    'american-football', 'boat', 'bluetooth', 'build'];

    this.items = [];
    //let swItems = this.starwarsService.getItems();
    //let person = this.starwarsService.getPerson(1);

    for (let i = 1; i < 20; i++)
    {
      this.starwarsService.load(i).subscribe(data => {
        this.items.push({
          title: data.name,
          note: data.homeworld,
          icon: data.gender
      });
    });
    }

    this.windowCount += 1;

    /*
    for(let i = 1; i < 50; i++) {
      this.items.push({
        title: swItems[i].title,
        note: swItems[i].notes,
        icon: swItems[i].icon
      });

      */
      /*
      this.items.push({
        title: 'Item ' + i,
        note: 'This is item #' + i,
        icon: this.icons[Math.floor(Math.random() * this.icons.length)]
      });
      */
    
  }

  itemTapped(event, item) {
    this.navCtrl.push(ItemDetailsPage, {
      item: item
    });
  }

    doInfinite(infiniteScroll)
    {
      console.log("begin async op");
      setTimeout(() => {
        let i = this.windowCount * 20;
        let max = i + 20;
        for ( ; i < max; i++)
        {
        this.starwarsService.load(i).subscribe(data => {
          this.items.push({
            title: data.name,
            note: data.homeworld,
            icon: data.gender
        });
      });

       
      }
      

      console.log("asyn complete");
      infiniteScroll.complete();
    }, 500);

     this.windowCount += 1;

    }
}
