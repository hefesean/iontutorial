import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { ListPage } from '../list/list';

import { LoginService } from '../services/LoginService';

@Component({
  selector: 'page-hello-ionic',
  templateUrl: 'hello-ionic.html'
})
export class HelloIonicPage {
	username: any;
	password: any;
	loggedIn: boolean;
	loginService: any;

  constructor(public navCtrl: NavController,private alertCtrl: AlertController,
  	loginService : LoginService) {
  	this.loggedIn = false;
  	this.loginService = loginService;

  	console.log("ctor" + this.loginService.getLoggedIn());
  }

  getLoggedIn()
  {
  	return this.loginService.getLoggedIn();
  }

  checkLogin() {
  	var success = false;
  	var msg = "";
  	if (this.username === "sean" && this.password === "sean")
  	{
  		success = true;
  		msg = "Login success";
  		this.loggedIn = true;
  		console.log("log in okay");
  		this.loginService.setLoggedIn(true);

		console.log("checkLogin" + this.loginService.getLoggedIn());
  
  	}
  	else {
  		msg = "Login failed. Try again";
  		console.log("log in failed");
  	}

	let alert = this.alertCtrl.create({
    	title: 'Login Attempt',
    	subTitle: msg,
    	buttons: ['Dismiss']
  	});

  alert.present();
  if (success === true) {
  	this.navCtrl.setRoot(ListPage);
	}
}

	signOff()
	{
		this.loginService.setLoggedIn(false);

	}



}
